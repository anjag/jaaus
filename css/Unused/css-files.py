##https://stackoverflow.com/questions/44156891/using-items-in-list-to-create-file-name-for-loop
mylist = [
'aplite_styles.css' ,
'gradient.css',
'JA_Australia_July2017-modules.css ' ,
'JA_Australia_July2017-style.css' ,
'LO_core.css' ,
'LO_Launch_Theme.css' ,
'LO_styles.css' ,
'News_Blog_by_PSDtoHUBSPOT.css' ,
'NGO-Header-Footer.css' ,
'Project-Donate-About-2.css' 
'Project-Donate-Aboutus.css' ,
'Project-Donate-Contact-Us.css' ,
'Project-Donate-Email-Subscription.css', 
'Project-Donate-FAQs.css' ,
'Project-Donate-Generic-Page.css' ,
'Project-Donate-Join_As_Volunteer.css' ,
'Project-Donate-Resource.css' ,
'Project-Donate-Team.css' ,
'Project-Donate-Three-Column.css' ,
'Project_Donate-Download-Ebook.css'
]

for word in mylist:
    with open(format(word), 'a') as f_output:
        print(' ', file=f_output)    
