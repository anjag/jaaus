/* FormPopup.js  */

<div style="text-align: center;">
  <span style="color: #ffffff;">
    <button class="jaGreenButton">
      <a href="https://www.jaaustralia.org/getinvolved/students?category=uniStudent" style="color: #ffffff;">
        Get Involved as a Uni Student
      </a>
    </button>
  </span>
</div>

<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
<script type="text/javascript">// <![CDATA[
// Get URL
  var url = window.location.href;
  // Check if URL contains the keyword to autofill form category
  if( url.search( '=' ) > 0 ) 
  {
    // Display the Element if true
    hbspt.forms.create(
      {
        portalId: "3008682",
        formId: "a45bd6f3-2eb5-432a-998e-6eeb67c1f532"
      }
    );
  }
// ]]></script>
<script>// <![CDATA[

// ]]></script>